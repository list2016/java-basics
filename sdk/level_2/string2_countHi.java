package level_2;

/*Return the number of times that the string "hi" appears anywhere in the given string.

countHi("abc hi ho") → 1
countHi("ABChi hi") → 2
countHi("hihi") → 2*/

public class string2_countHi {
    public static void main(String[] args) {
        int count = 0;
        char h ='h';
        char c = 'i';
        String str = "ancnc hi";
            for(int i = 0;i< str.length() - 1;i++){
                if (str.charAt(i) == h & str.charAt(i+1) == c){
                    count++;
                }
            }
        System.out.println(count);
    }
}
