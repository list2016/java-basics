package level_2;

/*Given 3 int values, a b c, return their sum.
However, if one of the values is 13 then it does not count
towards the sum and values to its right do not count.
So for example, if b is 13, then both b and c do not count.

luckySum(1, 2, 3) → 6
luckySum(1, 2, 13) → 3
luckySum(1, 13, 3) → 1*/

public class logic2_luckySum {
    public static void main(String[] args) {
        int a = 1, b = 2, c = 13;
        if (a == 13) System.out.println(0);
        else if (b == 13) System.out.println(a);
        else if (c == 13) System.out.println(a+b);
        else System.out.println(a+b+c);
    }
}
