package level_2;

/*Given an array length 1 or more of ints, return the difference between the
 largest and smallest values in the array. Note: the built-in Math.min(v1, v2)
 and Math.max(v1, v2) methods return the smaller or larger of two values.

bigDiff([10, 3, 5, 6]) → 7
bigDiff([7, 2, 10, 9]) → 8
bigDiff([2, 10, 7, 2]) → 8 */

public class array2_countEvens {
    public static void main(String[] args) {
        int [] nums = {2,1,2,4};
        int count = 0;
        for (int i = 0; i < nums.length; i++){
            int ost = nums[i] % 2;
            if (ost == 0){
                count++;
            }
        }
        System.out.println(count);

    }
}
