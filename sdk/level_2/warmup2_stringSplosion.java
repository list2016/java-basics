package level_2;

/*Given a non-empty string like "Code" return a string like "CCoCodCode".

stringSplosion("Code") → "CCoCodCode"
stringSplosion("abc") → "aababc"
stringSplosion("ab") → "aab"*/

public class warmup2_stringSplosion {
    public static void main(String[] args) {
        String str = "abc";
        String str1 = "";
        String str2 = "";
            for (int i = 0; i < str.length(); i++) {
                str2 = str2 + str.charAt(i);
                str1 = str1.concat(str2);
            }
        System.out.println(str1);
    }
}
