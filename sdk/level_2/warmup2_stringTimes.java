package level_2;

/*Given a string and a non-negative int n, return a larger string that is n copies of the original string.

stringTimes("Hi", 2) → "HiHi"
stringTimes("Hi", 3) → "HiHiHi"
stringTimes("Hi", 1) → "Hi"*/

public class warmup2_stringTimes {
    public static void main(String[] args) {
        String text = "";
        String str = "Hi";
        int n = 2;
            for (int i = 0; i < n; i++){
                text = text + str;
            }
        System.out.println(text);
    }
}
