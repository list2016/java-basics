package level_1;

/* Given an array of ints, return true if 6 appears
 as either the first or last element in the array.
  The array will be length 1 or more.

firstLast6([1, 2, 6]) → true
firstLast6([6, 1, 2, 3]) → true
firstLast6([13, 6, 1, 2, 3]) → false*/

public class array1_firstLast6 {
    public static void main(String[] args) {
        int[] nums = {1,2,6};
        int size = nums.length;
            if (nums[0] == 6 | nums[size -1] == 6){
                System.out.println(true);
            }
            else System.out.println(false);

    }
}
