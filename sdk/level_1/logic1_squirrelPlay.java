package level_1;

/*The squirrels in Palo Alto spend most of the day playing.
In particular, they play if the temperature is between 60 and 90 (inclusive).
Unless it is summer, then the upper limit is 100 instead of 90.
Given an int temperature and a boolean isSummer, return true if the squirrels play and false otherwise.

squirrelPlay(70, false) → true
squirrelPlay(95, false) → false
squirrelPlay(95, true) → true*/

public class logic1_squirrelPlay {

    public static void main(String[] args) {
        int temp = 50;
        boolean isSummer = false;

        if (temp >= 60 & temp <= 90 ){
            System.out.println(true);
        }
        else if (temp >= 60 & temp <= 100 & isSummer == true) System.out.println(true);
        else System.out.println(false);
    }
}
