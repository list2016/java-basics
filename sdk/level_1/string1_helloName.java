package level_1;

/*Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".

helloName("Bob") → "Hello Bob!"
helloName("Alice") → "Hello Alice!"
helloName("X") → "Hello X!"*/

public class string1_helloName {
    public static void main(String[] args) {
        String name = "Bob";
        System.out.println("Hello " + name + "!");
    }

}



